stages:
- {{ _ }}

{{ _ }}:
  stage: {{ _ }}
  image: docker:stable-git
  services:
    - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - {{ _ "docker_tools" "setup_docker" }}
    - {{ _ }}_build_image
    - {{ _ }}_tag_image
    - {{ _ }}_push_image
  only:
    refs:
      - master
      - tags

.{{ _ }}_script: &{{ _ }}_script |

  function {{ _ }}_build_image() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
      echo "Logging to GitLab Container Registry with CI credentials..."
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
      echo ""
    fi

    if [[ -f Dockerfile ]]; then
      docker build -t "$CI_REGISTRY_IMAGE":latest .
    else
      echo "Dockerfile is not found."
      return 1
    fi
  }

  function {{ _ }}_tag_image() {
    echo "Tagging Dockerfile-based application..."
    docker tag "$CI_REGISTRY_IMAGE":latest "$CI_REGISTRY_IMAGE":"${CI_COMMIT_SHA:0:8}"
  }

  function {{ _ }}_push_image() {
    echo "Pushing to GitLab Container Registry..."
    docker push "$CI_REGISTRY_IMAGE"
    echo ""
  }

before_script:
- *{{ _ }}_script
