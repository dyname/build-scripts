.{{ _ }}_script: &{{ _ }}_script |

  function {{ _ }}_setup_docker() {
    if ! docker info &>/dev/null; then
      if [ -z "$DOCKER_HOST" ] && [ "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      fi
    fi
  }

before_script:
- *{{ _ }}_script
