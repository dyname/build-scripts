stages:
- {{ _ }}

{{ _ }}:
  stage: {{ _ }}
  image: docker:stable-git
  services:
  - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
  - {{ _ "docker_tools" "setup_docker" }}
  - {{ _ }}_generate_configs
  only:
    refs:
    - master
  artifacts:
    expire_in: 1h
    paths:
    - output/
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
    - output/

.{{ _ }}_script: &{{ _ }}_script |

  function {{ _ }}_generate_configs() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
      echo "Logging to GitLab Container Registry with CI credentials..."
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
      echo ""
    fi

    mkdir -p output

    docker run -v "$(pwd)":/conf \
      registry.gitlab.com/dyname/glucose generate \
      --path /conf
  }

before_script:
  - *{{ _ }}_script
