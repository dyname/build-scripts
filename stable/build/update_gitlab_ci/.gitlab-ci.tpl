stages:
- {{ _ }}

{{ _ }}:
  stage: {{ _ }}
  script:
  - {{ _ }}_setup_git_client
  - {{ _ }}_push_config
  only:
    refs:
    - master

.{{ _ }}_script: &{{ _ }}_script |

  function {{ _ }}_setup_git_client() {
    command -v ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
    eval $(ssh-agent -s)

    if [[ -z ${SSH_PRIVATE_KEY+x} ]]; then
      echo "SSH_PRIVATE_KEY is not provided";
      exit 1
    else
      echo "Adding SSH key ..."
      echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    fi

    git config --global user.name {{ .Values.git.user.name }}
  {{- if .Values.git.user.email }}
    git config --global user.email {{ .Values.git.user.email }}
  {{- end }}

    mkdir -p ~/.ssh
    ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
  }

  function {{ _ }}_push_config() {

  {{- if .Values.project.repo }}
    git clone {{ .Values.project.repo }} repo
  {{- else }}
    echo "Git repository is not specified"
    exit 1
  {{- end }}

    cp ".gitlab-ci.yml" repo/.gitlab-ci.yml

    cd repo || exit 1

    if [[ $(git status --porcelain) ]]; then
      git commit -a -m "Updating .gitlab-ci.yml in {{ .Values.project.repo }}"
      git push origin master
    else
      echo "No changes in .gitlab-ci.yml skipping git commit ..."
    fi
  }

before_script:
- *{{ _ }}_script
